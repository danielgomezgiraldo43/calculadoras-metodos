#Librerías necesarias para la ejecución
import sympy as sp
from sympy import *

#Línea que marca que x va a ser tratado como un símbolo
x,y = sp.symbols('x y')

print("Este programa encuentra una raíz, por el método de regula falsi")


#Función para reemplazar x por un número y retornar el valor de la función
def f(funcion, a):
    #La función subs reemplaza el símbolo x por el valor de a y lo retorna
    return funcion.subs(x,a)

def calc_regla_falsa(xi,xs,tol,funcion):

    xa = xs - (xs-xi)/(f(funcion,xs)-f(funcion,xi)) * f(funcion,xs)
    i = 0
    raiz=[]
    raiz.insert(0,0)
    error = 1
    hay = True
    print('{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}'.format(
        'i', 'a', 'b', 'f(a)', 'f(b)', 'r', 'f(r)','error'))

    while abs(error) > tol:
        i = i + 1
        print('{:^10}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.6f}{:^10.6f}'.format(
            i, float(xi), float(xs), float(f(funcion,xi)), float(f(funcion,xs)), float(xa), float(f(funcion,xa)), abs(error)))
        if f(funcion,xi) * f(funcion,xa) < 0:
            xs = xa
        else:
            xi = xa
        xa = xs - (xs-xi)/(f(funcion,xs)-f(funcion,xi)) * f(funcion,xs)
        raiz.append(xa)
        error=(raiz[i]-raiz[i-1])/raiz[i]
        if i == 50:
            print("Número de iteraciones máximo alcanzado")
            hay = False
            break

    if hay:
        print('{:^10}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.6f}{:^10.6f}'.format(
            i+1, float(xi), float(xs), float(f(funcion,xi)), float(f(funcion,xs)), float(xa), float(f(funcion,xa)), abs(error)))
        print("La raíz ",xa," se halló en ",i+1," iteraciones")

#Input de la función. Sympify convierte strings en operaciones matemáticas que sympy puede usar
ec = sp.sympify(input('Ingrese la función f(x) = '))
xi = float(sp.sympify(input('Introduce el primer valor del intérvalo (a): ')))  #limite inferior
xs = float(sp.sympify(input('Introduce el segundo valor del intérvalo (b): ')))  #limite superior
tol = float((input('Introduce el error: ')))

calc_regla_falsa(xi,xs,tol,ec)
