#Función que verifica si el número es entero
def esEntero(number):
    try:
        int(number)
        return True
    except ValueError:
        return False

#Función de conversión de decimal a binario
def DecToBin(number):
    if float(number) > 0:
        decimal = number.split(".")

        entero = bin(int(decimal[0], base = 10))[2:]

        if esEntero(number):
            print("Binario = "+entero)

        else :
            fraccionario = float("0."+decimal[1])
            num = []
            for x in range(20):
                fraccionario = fraccionario*2
                fracc = str(fraccionario)[:1]
                fraccionario = float("0."+str(fraccionario)[2:])
                num.append(fracc)

            print("En Binario: "+entero+"."+''.join(num))
    
    else:
        decimal = number.split(".")
        entero = bin(int(decimal[0], base = 10))

        if esEntero(number):
            print("Binario = "+entero[0:1]+entero[3:])
        else: 
            fraccionario = float("0."+decimal[1])
            num = []

            for x in range(20):
                fraccionario = fraccionario*2
                fracc = str(fraccionario)[:1]
                fraccionario = float("0."+str(fraccionario)[2:])
                num.append(fracc)
            
            print("Binario = "+entero[0:1]+entero[3:]+"."+''.join(num))

#Función que verifica que el número sea válido
def esNumero(number):
    try:
        val = int(number, base=10)
        return True
    except ValueError:
        try:
            val = float(number)
            return True
        except ValueError:
            return False

#Main
number = input("Digite el número decimal:\n")
if esNumero(number):
    DecToBin(number)
else:
    print("No es un número")
    exit()