import sympy as sp
from math import *
x,y = sp.symbols('x y')

def f(funcion,a):
    return funcion.subs(x,a)

def calc_secante(x0,x1,tol,funcion):

    raiz=[]
    raiz.insert(0,0)
    i = 0
    x2 = 0
    error = 1
    hay = True
    print('{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}'.format(
        'iteración', 'límite 1', 'límite 2', 'raíz','f(raíz)','error'))
    while abs(error) > tol:
        x2 = x1 - (f(funcion,x1)*(x1-x0))/(f(funcion,x1)-f(funcion,x0))
        raiz.append(x2)
        print('{:^10}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.7f}'.format(
            i+1, float(x1), float(x0), float(x2),float(f(funcion,x2)),abs(error)))
        x0 = x1
        x1 = x2
        i += 1
        error=(raiz[i]-raiz[i-1])/raiz[i]
        if i == 50:
            print("Número de iteraciones máximo alcanzado")
            hay = False
            break

    if hay:
        print('{:^10}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.7f}'.format(
            i+1, float(x1), float(x0), float(x2),float(f(funcion,x2)),abs(error)))

        print('La raíz %r fue hallada en %f iteraciones'%(x2,i+1))

print ("Método de la secante")
ec = sp.sympify(input('Ingrese la función f(x) = '))
x0 = float(sp.sympify(input('Introduce el valor de inicio x0: ')))
x1 = float(sp.sympify(input('Introduce el valor de inicio x1: ')))
tol=float(input('Introduce el error '))

calc_secante(x0,x1,tol,ec)