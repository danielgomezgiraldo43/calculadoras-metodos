from sympy import *
import sympy as sp
# We have to create a "symbol" called x
x = Symbol('x')

f = sp.sympify(input('Ingrese la función f(x) = '))
p = Derivative(f, x, evaluate=True)
s = Derivative(p, x, evaluate=True)
#f_prime = f.diff(x)
punto=float(input("Punto a evaluar: "))

function = lambdify(x, f)
dFunction = lambdify(x, p)
ddFunction = lambdify(x, s)
# Let's test it out
print("Función: ", f)
print("Valor de la función en el punto indicado: " ,function(punto))
print("Primera derivada: ", p)
print("Valor de la primera derivada en el punto indicado: ",dFunction(punto))
print("Segunda derivada: ", s)
print("Valor de la segunda derivada en el punto indicado: ",ddFunction(punto))