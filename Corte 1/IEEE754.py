def iee32(binary):
    res = None
    for i in range(0, len(binary)): 
        if binary[i] == ".": 
            res = i + 1
            coma = res-2
            break
    
    cero = "0"

    if res == None: 

        if binary[0] == "-": 
            signo=1
        else:
            signo=0
    
        mat=binary.replace('-', '')

        exp=127+(len(mat)-1)
        exponencial=DecToBin(str(exp))

        if (len(mat)<24):
            for i in range(len(mat), 24):
                mat=mat+cero
            print("32 bits: "+str(signo)+"|"+exponencial+"|"+mat[1:24])
        else:
            mantisa=mat[1:24]
            print("32 bits: "+str(signo)+"|"+exponencial+"|"+mantisa)
    
    else: 

        if binary[0] == "-": 
            signo=1
            exp=(coma-1)+127
            exponencial=DecToBin(str(exp))
        else:
            signo=0
            exp=coma+127
            exponencial=DecToBin(str(exp))
    
        mat=binary.replace('-', '')
        matisa=mat.replace('.', '')


        if (len(matisa)<24):
            for i in range(len(matisa), 24):
                matisa=matisa+cero
            print("32 bits: "+str(signo)+"|"+exponencial+"|"+matisa[1:24])
        else:
            mantisa=matisa[1:24]
            print("32 bits: "+str(signo)+"|"+exponencial+"|"+mantisa)

def iee64(binary):
    res = None
    for i in range(0, len(binary)): 
        if binary[i] == ".": 
            res = i + 1
            coma = res-2
            break
    
    cero = "0"

    if res == None: 

        if binary[0] == "-": 
            signo=1
        else:
            signo=0
    
        mat=binary.replace('-', '')

        exp=1023+(len(mat)-1)
        exponencial=DecToBin(str(exp))

        if (len(mat)<53):
            for i in range(len(mat), 53):
                mat=mat+cero
            print("64 bits: "+str(signo)+"|"+exponencial+"|"+mat[1:53])
        else:
            mantisa=mat[1:53]
            print("64 bits: "+str(signo)+"|"+exponencial+"|"+mantisa)
    
    else: 

        if binary[0] == "-": 
            signo=1
            exp=(coma-1)+1023
            exponencial=DecToBin(str(exp))
        else:
            signo=0
            exp=coma+1023
            exponencial=DecToBin(str(exp))
    
        mat=binary.replace('-', '')
        matisa=mat.replace('.', '')


        if (len(matisa)<53):
            for i in range(len(matisa), 53):
                matisa=matisa+cero
            print("64 bits: "+str(signo)+"|"+exponencial+"|"+matisa[1:53])
        else:
            mantisa=matisa[1:53]
            print("64 bits: "+str(signo)+"|"+exponencial+"|"+mantisa)

def esEntero(number):
    try:
        int(number)
        return True
    except ValueError:
        return False

#Función de conversión de decimal a binario
def DecToBin(number):
    if float(number) > 0:
        decimal = number.split(".")

        entero = bin(int(decimal[0], base = 10))[2:]

        if esEntero(number):
            return entero

        else :
            fraccionario = float("0."+decimal[1])
            num = []
            for x in range(20):
                fraccionario = fraccionario*2
                fracc = str(fraccionario)[:1]
                fraccionario = float("0."+str(fraccionario)[2:])
                num.append(fracc)

            return entero+"."+''.join(num)
    
    else:
        decimal = number.split(".")
        entero = bin(int(decimal[0], base = 10))

        if esEntero(number):
            return entero[0:1]+entero[3:]
        else: 
            fraccionario = float("0."+decimal[1])
            num = []

            for x in range(20):
                fraccionario = fraccionario*2
                fracc = str(fraccionario)[:1]
                fraccionario = float("0."+str(fraccionario)[2:])
                num.append(fracc)
            
            return entero[0:1]+entero[3:]+"."+''.join(num)

#Función que verifica que el número sea válido
def esNumero(number):
    try:
        val = int(number, base=10)
        return True
    except ValueError:
        try:
            val = float(number)
            return True
        except ValueError:
            return False

def es_bin(bin):
    bin_array = ["0","1"]
    puntos = 0
    a = 0
    neg = False
    menos = 0
    for i in bin:
        if i == ".":
            puntos+=1
        if i == "-" and bin.index(i) == 0:
            menos += 1
            neg = True
        elif i == "-" and bin.index(i) != 0:
            menos += 1
            neg = False
        for j in bin_array:
            if i == j:
                a+=1
    if menos == 1 and neg:
        if puntos == 1:
            if a == len(bin)-2:
                return True
            else:
                return False
        elif puntos == 0:
            if a == len(bin)-1:
                return True
            else:
                return False
        else:
            return False
    elif menos == 0:
        if puntos == 1:
            if a == len(bin)-1:
                return True
            else:
                return False
        elif puntos == 0:
            if a == len(bin):
                return True
            else:
                return False
        else:
            return False

def binaryToDecimal(binary, length) : 
    
    if "-" in binary:

        bin1 = "" 
  
        for i in range(len(binary)): 
            if i != 0: 
                bin1 = bin1 + binary[i] 

        length1=length-1

        # Busca el punto base  
        point = bin1.find('.') 
        # Actualizar el punto si no se encuentra 
        if (point == -1) : 
            point = length1  
  
        intDecimal = 0
        fracDecimal = 0
        twos = 1
  
        # Convertir la parte entera del binario 
        # a un decimal equivalente 
        for i in range(point-1, -1, -1) :  
          
            # Restar '0' para convertir 
            # en entero  
            intDecimal += ((ord(bin1[i]) - 
                            ord('0')) * twos)  
            twos *= 2
  
        # Convertir la parte fraccionaria del binario
        # a un decimal equivalente 
        twos = 2
      
        for i in range(point + 1, length1): 
          
            fracDecimal += ((ord(bin1[i]) -
                         ord('0')) / twos);  
            twos *= 2.0
  
        # Sumar la parte integral y fraccionaria  
        ans = intDecimal + fracDecimal 
        
        convert=str(ans)
        menos="-"
        res = list(convert) 
        res.insert(0, menos) 
        res = ''.join(res) 
        return res

    else:
        # Busca el punto base  
        point = binary.find('.') 
  
        # Actualizar el punto si no se encuentra 
        if (point == -1) : 
            point = length  
  
        intDecimal = 0
        fracDecimal = 0
        twos = 1
  
        # Convertir la parte entera del binario 
        # a un decimal equivalente 
        for i in range(point-1, -1, -1) :  
          
            # Restar '0' para convertir 
            # en entero  
            intDecimal += ((ord(binary[i]) - 
                            ord('0')) * twos)  
            twos *= 2
  
        # Convertir la parte fraccionaria del binario
        # a un decimal equivalente 
        twos = 2
      
        for i in range(point + 1, length): 
          
            fracDecimal += ((ord(binary[i]) -
                         ord('0')) / twos);  
            twos *= 2.0
  
        # Sumar la parte integral y fraccionaria  
        ans = intDecimal + fracDecimal 
      
        return ans

def fromDec():

    opcion = input("""
Digite 1 para introducir un número Decimal.
Digite 2 para introducir un número Binario.
""")

    signo = ""
    if int(opcion) == 1:
        number = input("Digite el número Decimal (sin espacios)\n")
        if esNumero(number):
            binary=DecToBin(number)
            print("Binario: ", binary)
            iee32(binary)
            iee64(binary)
        else:
            print("El número ingresado no es decimal")
    elif int(opcion) == 2:
        number = input("Digite el número Binario (sin espacios)\n")
        if es_bin(number):
            iee32(number)
            iee64(number)
        else:
            print("El número ingresado no es Binario")

def fromStandard():

    opcion = input("""
Digite 1 para introducir un número en IEEE 32 bits.
Digite 2 para introducir un número en IEEE 64 bits.
""")
    signo = ""
    if int(opcion) == 1:
        num = input("Digite el número en IEEE 32 bits (sin espacios)\n")
        if len(num)<=32 and es_bin(num):
            if len(num) < 32:
                num = num.ljust(32,'0')
            if num[0] == '0':
                signo = '+'
            else:
                signo = '-'
            mantisa = num[9:]
            exp = num[1:9]
            coma = binaryToDecimal(exp,len(exp))
            coma = coma - 127
            if coma > 23:
                mantisa = mantisa.ljust(coma,'0')
            binario = '1'+mantisa[:coma]+'.'+mantisa[coma:]
            decimal = binaryToDecimal(binario,len(binario))
            print("Decimal: ",decimal)
            print("Binario: ",binario)
        else:
            print("El número ingresado no está en estándar IEEE de 32 bits")
    elif int(opcion) == 2:
        num = input("Digite el número en IEEE 64 bits (sin espacios)\n")
        if es_bin(num) and len(num) <= 64:
            if len(num) < 64:
                num = num.ljust(64,'0')
            if num[0] == "0":
                signo = "+"
            else:
                signo = "-"
            exp = num[1:12]
            mantisa = num[12:]
            coma = binaryToDecimal(exp, len(exp))
            coma -= 1023
            if coma > 52:
                mantisa = mantisa.ljust(coma,'0')
            mantisa = mantisa[:coma]+'.'+mantisa[coma:]
            binario = "1"+mantisa
            decimal = binaryToDecimal(binario, len(binario))
            if signo == "-":
                decimal = -decimal
                binario = "-"+binario
            print("Decimal: ",decimal)
            print("Binario: "+binario)
        else:
            print("El número ingresado no está en estándar IEEE de 64 bits")



while True:

    opcion = input("""
Digite 1 para convertir de binario/decimal a estándar IEEE 754
Digite 2 para convertir de estándar IEEE 754 a binario/decimal
Digite otra tecla para salir
""")

    if opcion == "1":
        fromDec()
    elif opcion == "2":
        fromStandard()
    else:
        print("Adiós")
        break