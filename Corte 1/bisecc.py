import sympy as sp
from sympy import *
x,y = sp.symbols('x y')
signo = ""
limite = ""

print("Este programa encuentra una raíz, por el método de bisección")

def f(funcion, a):
    return funcion.subs(x,a)

def calc_Biseccion(xi,xs,tol,funcion):
    i = 0
    raiz=[]
    raiz.append(xs)
    error = 1
    hay = True
    xa = 0.0
    if f(funcion,xi)*f(funcion,xs) < 0:
        xa = (xi + xs) / 2.0
        print('{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}{:^10}'.format(
            'i', 'a', 'b', 'f(a)', 'f(b)', 'r', 'f(r)','error'))

        while abs(error) > tol:
            i = i + 1
            print('{:^10}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.6f}{:^10.6f}'.format(
                i, float(xi), float(xs), float(f(funcion,xi)), float(f(funcion,xs)), float(xa), float(f(funcion,xa)), abs(error)))
            if f(funcion,xi) * f(funcion,xa) < 0:
                xs = xa
                signo = "negativo"
                limite = "superior"
            else:
                xi = xa
                signo = "positivo"
                limite = "inferior"
            xa = (xi + xs) / 2.0
            raiz.append(xa)
            error=(raiz[i-1]-raiz[i])/raiz[i]
            if i == 50:
                hay = False
                break
        if hay:
            print('{:^10}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.4f}{:^10.6f}{:^10.6f}'.format(
            i+1, float(xi), float(xs), float(f(funcion,xi)), float(f(funcion,xs)), float(xa), float(f(funcion,xa)), abs(error)))
            print("La raíz ",xa," se halló en ",i+1," iteraciones",' con un error relativo de ',abs(error))
        else:
            print("Número de iteraciones máximo alcanzado")
    else:
        print("Las imágenes de los puntos tienen el mismo signo, no se puede calcular la raíz por este método")
    

ec = sp.sympify(input('Ingrese la función f(x) = '))
xi = float(sp.sympify(input('Introduce el primer valor del intérvalo (a): ')))  #limite inferior
xs = float(sp.sympify(input('Introduce el segundo valor del intérvalo (b): ')))  #limite superior
tol = float((input('Introduce el error: ')))

calc_Biseccion(xi,xs,tol,ec)