import numpy as np
from numpy.polynomial import Polynomial as P
import array as arr

print ("\nRaices de polinomios")
max=int(input("Ingrese el grado del polinomio: "))

a = arr.array('d', (0 for i in range(0,max+1)))
i=0
for n in a:
    a[i]=float(input("ingrese el coeficiente de x^%s " % (i)))
    i+=1
coeff = P(a)

print("\nRaices halladas:")
for r in coeff.roots():
    print(r)

# Redondear a 8 decimales significativos