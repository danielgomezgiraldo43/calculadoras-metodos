import sympy as sp
from sympy import *
x,y = sp.symbols('x y')

print ("Método de Newton-Raphson")
def f(funcion,a):
    return funcion.subs(x,a)

def deri(eq,a):
    return Derivative(eq,x,evaluate=True).subs(x,a)

def calc_Newton(x0,tol,ec):
    raiz=[]
    raiz.insert(0,0)
    i = 0
    error = 1
    hay = True

    print('{:^12}{:^12}{:^12}{:^12}'.format(
        'iteración', 'punto inicial', 'raíz','error'))

    while abs(error) > tol:
        x1=x0-(f(ec,x0)/deri(ec,x0))
        raiz.append(x1)
        print('{:^12}{:^12.6f}{:^12.6f}{:^12.12f}'.format(
            i+1, float(x0), float(x1),abs(error)))
        i += 1
        x0 = x1
        error=(raiz[i]-raiz[i-1])/raiz[i]
        # print (x0)
        if i == 50:
            print("Número de iteraciones máximo alcanzado")
            hay = False
            break

    if hay:
        print('{:^12}{:^12.6f}{:^12.6f}{}'.format(
            i+1, float(x1), float(x0),abs(error)))
        print('La raíz %r fue hallada en %f iteraciones con un error de %e'%(x1,i+1,abs(error)))

ec = sp.sympify(input('Ingrese la función f(x) = '))
x0 = float(sp.sympify(input('Introduce el valor de inicio ')))
tol = float(input('Introduce el error '))

calc_Newton(x0,tol,ec)