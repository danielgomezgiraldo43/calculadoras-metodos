import numpy as np
from matplotlib import pyplot as plt
from math import *

i = 0
a_arr = []
b_arr = []
raices = []
errores = []

def rfalsa(f,a,b,err,max_it=50):
    xh = 0
    pos = 0
    raiz = True
    if f(a) * f(b) < 0:
        for pos in range(1,max_it+1):
            xh = b - (b-a)/(f(b)-f(a)) * f(b)
            a_arr.append(a)
            b_arr.append(b)
            raices.append(xh)
            errores.append(abs(f(xh)))
            if abs(f(xh)) < err: break
            elif f(a) * f(xh) < 0:
                b = xh
            else:
                a = xh
    else:
        print("No hay raíces en ese intervalo, o hay más de una raíz")
        raiz = False
    return xh,pos,raiz


y = lambda x: x**2 - 5

a = float(input("Ingrese el primer valor del intérvalo (a): "))
b = float(input("Ingrese el segundo valor del intérvalo (b): "))
err = float(input("Ingrese el valor del error de tolerancia: "))
x = range(-2,5)

r,n,found = rfalsa(y,a,b,err)
if found:
    print('La raíz %f fue hallada en %d iteraciones'%(r,n))
    print('|\t\ta\t\t|\t\tb\t\t|\t\tr\t\t|\t\terror\t\t|')
    # print('{:^10}{:^10}{:^10}{:^10}'.format(
    # 'a','b','raiz','error')
    for i in range(n):
        print('|\t\t'+str(a_arr[i])+'\t\t|\t\t'+str(b_arr[i])+'\t\t|\t\t'+str(raices[i])+'\t\t|\t\t'+str(errores[i])+'\t\t|')
        # print('{:^10}{:^10.4f}{:^10.4f}{:^10.4f}{:^10}{:^10}{:^10.4f}'.format(
        # a_arr[i], b_arr[i], raices[i], errores[i])

    plt.plot(x,[y(i) for i in x])
    plt.axhline(0, color="black")
    plt.axvline(0, color="black")
    plt.grid()
    plt.show()

