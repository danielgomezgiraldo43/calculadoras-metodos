def createMap(bin_oct_map): 
    bin_oct_map["000"] = '0'
    bin_oct_map["001"] = '1'
    bin_oct_map["010"] = '2'
    bin_oct_map["011"] = '3'
    bin_oct_map["100"] = '4'
    bin_oct_map["101"] = '5'
    bin_oct_map["110"] = '6'
    bin_oct_map["111"] = '7'

def createMap1(bin_hex_map): 
    bin_hex_map["0000"] = '0'
    bin_hex_map["0001"] = '1'
    bin_hex_map["0010"] = '2'
    bin_hex_map["0011"] = '3'
    bin_hex_map["0100"] = '4'
    bin_hex_map["0101"] = '5'
    bin_hex_map["0110"] = '6'
    bin_hex_map["0111"] = '7'
    bin_hex_map["1000"] = '8'
    bin_hex_map["1001"] = '9'
    bin_hex_map["1010"] = 'A'
    bin_hex_map["1011"] = 'B'
    bin_hex_map["1100"] = 'C'
    bin_hex_map["1101"] = 'D'
    bin_hex_map["1110"] = 'E'
    bin_hex_map["1111"] = 'F'

def es_bin(bin):
    bin_array = ["0","1"]
    puntos = 0
    a = 0
    neg = False
    menos = 0
    for i in bin:
        if i == ".":
            puntos+=1
        if i == "-" and bin.index(i) == 0:
            menos += 1
            neg = True
        elif i == "-" and bin.index(i) != 0:
            menos += 1
            neg = False
        for j in bin_array:
            if i == j:
                a+=1
    if menos == 1 and neg:
        if puntos == 1:
            if a == len(bin)-2:
                return True
            else:
                return False
        elif puntos == 0:
            if a == len(bin)-1:
                return True
            else:
                return False
        else:
            return False
    elif menos == 0:
        if puntos == 1:
            if a == len(bin)-1:
                return True
            else:
                return False
        elif puntos == 0:
            if a == len(bin):
                return True
            else:
                return False
        else:
            return False
  
# Function to find octal equivalent of binary  
def convertBinToOct(bin): 

    if "-" in bin:

        bin1 = "" 
  
        for i in range(len(bin)): 
            if i != 0: 
                bin1 = bin1 + bin[i] 

        l = len(bin1)
      
        # length of string before '.'  
        t = -1
        if '.' in bin1: 
            t = bin1.index('.')  
            len_left = t 
        else: 
            len_left = l  
      
        # add min 0's in the beginning to make  
        # left substring length divisible by 3  
        for i in range(1, (3 - len_left % 3) % 3 + 1): 
            bin1 = '0' + bin1
      
        # if decimal point exists  
        if (t != -1):  
          
            # length of string after '.'  
            len_right = l - len_left - 1
          
            # add min 0's in the end to make right  
            # substring length divisible by 3  
            for i in range(1, (3 - len_right % 3) % 3 + 1): 
                bin1 = bin1 + '0'
      
        # create dictionary between binary and its  
        # equivalent octal code  
        bin_oct_map = {} 
        createMap(bin_oct_map) 
        i = 0
        octal = "" 
      
        while (True) : 
          
            # one by one extract from left, substring  
            # of size 3 and add its octal code  
            octal += bin_oct_map[bin1[i:i + 3]]  
            i += 3
            if (i == len(bin1)):  
                break
              
            # if '.' is encountered add it to result  
            if (bin1[i] == '.'): 
                octal += '.'
                i += 1
              
        # required octal number  
        menos="-"
        res = list(octal) 
        res.insert(0, menos) 
        res = ''.join(res) 
        return res
    else:
        l = len(bin)
      
        # length of string before '.'  
        t = -1
        if '.' in bin: 
            t = bin.index('.')  
            len_left = t 
        else: 
            len_left = l  
      
        # add min 0's in the beginning to make  
        # left substring length divisible by 3  
        for i in range(1, (3 - len_left % 3) % 3 + 1): 
            bin = '0' + bin
      
        # if decimal point exists  
        if (t != -1):  
          
            # length of string after '.'  
            len_right = l - len_left - 1
          
            # add min 0's in the end to make right  
            # substring length divisible by 3  
            for i in range(1, (3 - len_right % 3) % 3 + 1): 
                bin = bin + '0'
      
        # create dictionary between binary and its  
        # equivalent octal code  
        bin_oct_map = {} 
        createMap(bin_oct_map) 
        i = 0
        octal = "" 
      
        while (True) : 
          
            # one by one extract from left, substring  
            # of size 3 and add its octal code  
            octal += bin_oct_map[bin[i:i + 3]]  
            i += 3
            if (i == len(bin)):  
                break
              
            # if '.' is encountered add it to result  
            if (bin[i] == '.'): 
                octal += '.'
                i += 1
    return octal

def convertBinToHex(bin): 
    
    if "-" in bin:
        bin1 = "" 
  
        for i in range(len(bin)): 
            if i != 0: 
                bin1 = bin1 + bin[i] 

        l = len(bin1)

        t = -1
        if '.' in bin1: 
            t = bin1.index('.')  
            len_left = t 
        else: 
            len_left = l   
      
        # add min 0's in the beginning to make 
        # left substring length divisible by 4  
        for i in range(1, (4 - len_left % 4) % 4 + 1):
            bin1 = '0' + bin1
      
        # if decimal point exists     
        if (t != -1):   
     
            # length of string after '.' 
            len_right = l - len_left - 1
          
            # add min 0's in the end to make right 
            # substring length divisible by 4  
            for i in range(1, (4 - len_right % 4) % 4 + 1):
                bin1 = bin1 + '0'
      
        # create map between binary and its 
        # equivalent hex code 
        bin_hex_map = {}
        createMap1(bin_hex_map)
      
        i = 0 
        hex = ""
      
        while (True): 
            # one by one extract from left, substring 
            # of size 4 and add its hex code 
            hex += bin_hex_map[bin1[i:i + 4]]
            i += 4
            if (i == len(bin1)): 
                break 
              
            # if '.' is encountered add it 
            # to result 
            if (bin1[i] == '.'):      
                hex += '.' 
                i+=1  
      
        # required hexadecimal number 
        menos="-"
        res = list(hex) 
        res.insert(0, menos) 
        res = ''.join(res) 
        return res
    
    else:
        l = len(bin) 
        t = -1
        if '.' in bin: 
            t = bin.index('.')  
            len_left = t 
        else: 
            len_left = l   
      
        # add min 0's in the beginning to make 
        # left substring length divisible by 4  
        for i in range(1, (4 - len_left % 4) % 4 + 1):
            bin = '0' + bin
      
        # if decimal point exists     
        if (t != -1):   
     
            # length of string after '.' 
            len_right = l - len_left - 1
          
            # add min 0's in the end to make right 
            # substring length divisible by 4  
            for i in range(1, (4 - len_right % 4) % 4 + 1):
                bin = bin + '0'
      
        # create map between binary and its 
        # equivalent hex code 
        bin_hex_map = {}
        createMap1(bin_hex_map)
      
        i = 0 
        hex = ""
      
        while (True): 
            # one by one extract from left, substring 
            # of size 4 and add its hex code 
            hex += bin_hex_map[bin[i:i + 4]]
            i += 4
            if (i == len(bin)): 
                break 
              
            # if '.' is encountered add it 
            # to result 
            if (bin[i] == '.'):      
                hex += '.' 
                i+=1  
      
        # required hexadecimal number 
        return hex

def binaryToDecimal(binary, length) : 
    
    if "-" in binary:

        bin1 = "" 
  
        for i in range(len(binary)): 
            if i != 0: 
                bin1 = bin1 + binary[i] 

        length1=length-1

        # Busca el punto base  
        point = bin1.find('.') 
        # Actualizar el punto si no se encuentra 
        if (point == -1) : 
            point = length1  
  
        intDecimal = 0
        fracDecimal = 0
        twos = 1
  
        # Convertir la parte entera del binario 
        # a un decimal equivalente 
        for i in range(point-1, -1, -1) :  
          
            # Restar '0' para convertir 
            # en entero  
            intDecimal += ((ord(bin1[i]) - 
                            ord('0')) * twos)  
            twos *= 2
  
        # Convertir la parte fraccionaria del binario
        # a un decimal equivalente 
        twos = 2
      
        for i in range(point + 1, length1): 
          
            fracDecimal += ((ord(bin1[i]) -
                         ord('0')) / twos);  
            twos *= 2.0
  
        # Sumar la parte integral y fraccionaria  
        ans = intDecimal + fracDecimal 
        
        convert=str(ans)
        menos="-"
        res = list(convert) 
        res.insert(0, menos) 
        res = ''.join(res) 
        return res

    else:
        # Busca el punto base  
        point = binary.find('.') 
  
        # Actualizar el punto si no se encuentra 
        if (point == -1) : 
            point = length  
  
        intDecimal = 0
        fracDecimal = 0
        twos = 1
  
        # Convertir la parte entera del binario 
        # a un decimal equivalente 
        for i in range(point-1, -1, -1) :  
          
            # Restar '0' para convertir 
            # en entero  
            intDecimal += ((ord(binary[i]) - 
                            ord('0')) * twos)  
            twos *= 2
  
        # Convertir la parte fraccionaria del binario
        # a un decimal equivalente 
        twos = 2
      
        for i in range(point + 1, length): 
          
            fracDecimal += ((ord(binary[i]) -
                         ord('0')) / twos);  
            twos *= 2.0
  
        # Sumar la parte integral y fraccionaria  
        ans = intDecimal + fracDecimal 
      
        return ans

def es_oct(num):
    oct_array = ["0","1","2","3","4","5","6","7"]
    puntos = 0
    a = 0
    neg = False
    menos = 0
    for i in num.lower():
        if i == ".":
            puntos+=1
        if i == "-" and num.index(i) == 0:
            menos += 1
            neg = True
        elif i == "-" and num.index(i) != 0:
            menos += 1
            neg = False
        for j in oct_array:
            if i == j:
                a+=1
    if (menos == 1 and neg):
        if puntos == 1:
            if a == len(num)-2:
                return True
            else:
                return False
        elif puntos == 0:
            if a == len(num)-1:
                return True
            else:
                return False
        else:
            return False
    if menos == 0:
        if puntos == 1:
            if a == len(num)-1:
                return True
            else:
                return False
        elif puntos == 0:
            if a == len(num):
                return True
            else:
                return False
        else:
            return False


def octToBinary(num):
    conv = ['000','001','010','011','100','101','110','111']
    num = num.lower()
    neg = False
    if num[0] == "-":
        neg = True
    punto = -1
    for i in num:
        if i == ".":
            punto = num.index(".")

    bin_conv = ""

    if punto == -1:
        if neg:
            num = num.replace("-","")
        for i in num:
            if int(i) >= 0 and int(i) <= 7:
                bin_conv += conv[int(i)]

    if punto != -1:
        if neg:
            num = num.replace("-","")
        separado = num.split(".",1)
        izq = separado[0]
        der = separado[1]
        bin_conv = ""

        for i in izq:
            if int(i) >= 0 and int(i) <= 7:
                bin_conv += conv[int(i)]
        
        bin_conv += "."

        for i in der:
            if int(i) >= 0 and int(i) <= 7:
                bin_conv += conv[int(i)]

    if neg:
        bin_conv = "-"+bin_conv
    
    return bin_conv

def es_hex(num):
    hex_array = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"]
    puntos = 0
    a = 0
    neg = False
    menos = 0
    for i in num.lower():
        if i == ".":
            puntos+=1
        if i == "-" and num.index(i) == 0:
            menos += 1
            neg = True
        elif i == "-" and num.index(i) != 0:
            menos += 1
            neg = False
        for j in hex_array:
            if i == j:
                a+=1
    if (menos == 1 and neg):
        if puntos == 1:
            if a == len(num)-2:
                return True
            else:
                return False
        elif puntos == 0:
            if a == len(num)-1:
                return True
            else:
                return False
        else:
            return False
    if menos == 0:
        if puntos == 1:
            if a == len(num)-1:
                return True
            else:
                return False
        elif puntos == 0:
            if a == len(num):
                return True
            else:
                return False
        else:
            return False


def hexToBinary(num):
    conv = ['0000','0001','0010','0011','0100','0101','0110','0111','1000','1001','1010','1011','1100','1101','1110','1111']
    num = num.lower()
    neg = False
    if num[0] == "-":
        neg = True
    punto = -1
    for i in num:
        if i == ".":
            punto = num.index(".")

    bin_conv = ""

    if punto == -1:
        if neg:
            num = num.replace("-","")
        for i in num:
            if i == "a":
                bin_conv += conv[10]
            elif i == "b":
                bin_conv += conv[11]
            elif i == "c":
                bin_conv += conv[12]
            elif i == "d":
                bin_conv += conv[13]
            elif i == "e":
                bin_conv += conv[14]
            elif i == "f":
                bin_conv += conv[15]
            elif int(i) >= 0 and int(i) <= 9:
                bin_conv += conv[int(i)]

    if punto != -1:
        if neg:
            num = num.replace("-","")
        separado = num.split(".",1)
        izq = separado[0]
        der = separado[1]
        bin_conv = ""

        for i in izq:
            if i == "a":
                bin_conv += conv[10]
            elif i == "b":
                bin_conv += conv[11]
            elif i == "c":
                bin_conv += conv[12]
            elif i == "d":
                bin_conv += conv[13]
            elif i == "e":
                bin_conv += conv[14]
            elif i == "f":
                bin_conv += conv[15]
            elif int(i) >= 0 and int(i) <= 9:
                bin_conv += conv[int(i)]
        
        bin_conv += "."

        for i in der:
            if i == "a":
                bin_conv += conv[10]
            elif i == "b":
                bin_conv += conv[11]
            elif i == "c":
                bin_conv += conv[12]
            elif i == "d":
                bin_conv += conv[13]
            elif i == "e":
                bin_conv += conv[14]
            elif i == "f":
                bin_conv += conv[15]
            elif int(i) >= 0 and int(i) <= 9:
                bin_conv += conv[int(i)]

    if neg:
        bin_conv = "-"+bin_conv
    
    return bin_conv

def convertDecimal():
    print("Digite el número decimal:\n")

    number = str(input())

    def esEntero(number):
        try:
            int(number)
            return True
        except ValueError:
            return False


    try:
        val = int(number, base=10)
    except ValueError:
        try:
            val = float(number)
        except ValueError:
            print("No es un número decimal")
            exit()

    if float(number)>0:
        #A BINARIO
        decimal = number.split(".")

        entero = bin(int(decimal[0], base = 10))[2:]

        

        if not esEntero(number):
            fraccionario = float("0."+decimal[1])
            num = []
            for x in range(20):
                fraccionario = fraccionario*2
                fracc = str(fraccionario)[:1]
                fraccionario = float("0."+str(fraccionario)[2:])
                num.append(fracc)

            print("En Binario: "+entero+"."+''.join(num))


        #A OCTAL
        octal = number.split(".")

        ent = oct(int(octal[0], base = 10))[2:]

        if not esEntero(number):
            fraccionario = float("0."+octal[1])
            arr = []
            for x in range(20):
                fraccionario = fraccionario*8
                fracc = str(fraccionario)[:1]
                fraccionario = float("0."+str(fraccionario)[2:])
                arr.append(fracc)

            print("En Octal: "+ent+"."+''.join(arr))


        #A HEXADECIMAL
        numero = number.split(".")
        hexadecimalEntero = hex(int(numero[0], base = 10))[2:]

        if not esEntero(number):
            fraccionario = float("0."+numero[1])
            res = []
            for x in range(20):
                fraccionario = fraccionario*16
                fracc = str(fraccionario)
                conv = hex(int(fracc.split(".")[0], base = 10))[2:]
                fraccionario = float("0."+fracc.split(".")[1])
                res.append(conv)
                
            print("Hexadecimal = "+hexadecimalEntero+"."+''.join(res))
            exit()

        print("Binario = "+entero)
        print("Octal = "+ent)
        print("Hexadecimal = "+hexadecimalEntero)

    else:
        #A BINARIO
        decimal = number.split(".")
        entero = bin(int(decimal[0], base = 10))

        if not esEntero(number):
            fraccionario = float("0."+decimal[1])
            num = []

            for x in range(20):
                fraccionario = fraccionario*2
                fracc = str(fraccionario)[:1]
                fraccionario = float("0."+str(fraccionario)[2:])
                num.append(fracc)
            
            print("Binario = "+entero[0:1]+entero[3:]+"."+''.join(num))


        #A OCTAL
        octal = number.split(".")

        ent = oct(int(octal[0], base = 10))

        if not esEntero(number):
            fraccionario = float("0."+octal[1])
            arr = []

            for x in range(20):
                fraccionario = fraccionario*8
                fracc = str(fraccionario)[:1]
                fraccionario = float("0."+str(fraccionario)[2:])
                arr.append(fracc)

            print("Octal = "+ent[0:1]+ent[3:]+"."+''.join(arr))


        #A HEXADECIMAL
        numero = number.split(".")
        hexadecimalEntero = hex(int(numero[0], base = 10))

        if not esEntero(number):
            fraccionario = float("0."+numero[1])
            res = []

            for x in range(20):
                fraccionario = fraccionario*16
                fracc = str(fraccionario)
                conv = hex(int(fracc.split(".")[0], base = 10))[2:]
                fraccionario = float("0."+fracc.split(".")[1])
                res.append(conv)

            print("Hexadecimal = "+hexadecimalEntero[0:1]+hexadecimalEntero[3:]+"."+''.join(res))
            exit()


        print("Binario = "+entero[0:1]+entero[3:])
        print("Octal = "+ent[0:1]+ent[3:])
        print("Hexadecimal = "+hexadecimalEntero[0:1]+hexadecimalEntero[3:])

    #print("Binario ", bin(decimal))

while True:
    base = input("""
Bienvenido a la calculadora de bases, un proyecto de:
Daniel Gómez, Juan José Barrera, Mariana Velásquez, Camilo Andrés Rojas, Mª Alejandra García y Santiago Rodriguez.

Escoja una base y luego digite el número a convertir, la calculadora convertirá ese número a las demás bases.

Digite 1 para base binaria.
Digite 2 para base octal.
Digite 3 para base decimal.
Digite 4 para base hexadecimal.
Digite 5 para salir.\n
    """)

    if base == "1":
        bin = input("Digite el número binario:\n")
        if es_bin(bin):
            print("Decimal = ", binaryToDecimal(bin, len(bin)))
            print("Octal = ", convertBinToOct(bin))
            print("Hexadecimal = ", convertBinToHex(bin))
        else:
            print("El número ingresado no es un número binario")
    elif base == "2":
        num = input("Digite el número octal:\n")
        if es_oct(num):
        #    print ("El número ingresado SÍ es octadecimal")
            print("Binario = "+octToBinary(num))
            print("Decimal = ",binaryToDecimal(octToBinary(num),len(octToBinary(num))))
            print("Hexadecimal = "+convertBinToHex(octToBinary(num)))
        else: 
            print ("El número ingresado NO es octal")
    elif base == "3":
        convertDecimal()
    elif base == "4":

        num = input("Digite el número hexadecimal:\n")
        if es_hex(num):
        #    print ("El número ingresado SÍ es hexadecimal")
            print("Binario = "+hexToBinary(num))
            print("Octal = "+convertBinToOct(hexToBinary(num)))
            print("Decimal = ",binaryToDecimal(hexToBinary(num),len(hexToBinary(num))))
        else: 
            print ("El número ingresado NO es hexadecimal")
    else:
        print("¡Adiós!")
        break    
    
