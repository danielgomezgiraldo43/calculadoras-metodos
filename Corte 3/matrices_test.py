import numpy as np

def suma(a, b):
    return a + b

def resta(a, b):
    return a - b

def prodPunto(a, b):
    return a.dot(b)

def multEscalar(a, esc):
    return a*esc

def determinante(a):
    return np.linalg.det(a)

def inversa(a):
    return np.linalg.inv(a)

def transpuesta(a):
    return A.transpose()

# A basic code for matrix input from user 
  
n1 = int(input("Ingrese la dimensión 'n' de la matriz A 'nxn': "))
n2 = int(input("Ingrese la dimensión 'n' de la matriz B 'nxn': "))
  
# Initialize matrix 
matrixA = [] 

print("Para la matriz A")
# For user input 
for i in range(n1):          # A for loop for row entries 
    a =[] 
    for j in range(n1):      # A for loop for column entries 
        print("Posición [",i+1,"][",j+1,"]")
        a.append(float(input())) 
    matrixA.append(a) 

matrixB = [] 

print("Para la matriz B") 
# For user input 
for i in range(n2):          # A for loop for row entries 
    a =[] 
    for j in range(n2):      # A for loop for column entries 
        print("Posición [",i+1,"][",j+1,"]")
        a.append(float(input())) 
    matrixB.append(a) 

matrixA = np.array(matrixA)
matrixB = np.array(matrixB)
# For printing the matrix 
print("Matriz A:")
print(matrixA)
print("Matriz B:")
print(matrixB)

print("""
Digite una opción:

1. Hallar el determinante de A o B.
2. Sumar A y B.
3. Restar A y B.
4. Producto punto de A y B.
5. Multiplicar A o B por un escalar.
6. Inversa de A o B.
7. Transpuesta de A o B.

Digite otro número para salir.""")

opcion = (input())

if opcion == '1':
    opMat = (input("Digite 1 para hallar el determinante de A o 2 para el de B: "))
    if opMat == '1':
        print("Determinante de A: ")
        print(determinante(matrixA))
    elif opMat == '2':
        print("Determinante de A: ")
        print(determinante(matrixB))
    else:
        print("Opción no válida")
elif opcion == '2':
    if n1 == n2:
        print("Suma de A y B: ")
        print(suma(matrixA, matrixB))
    else:
        print("Las matrices no tienen la misma dimensión, no es posible sumar")
elif opcion == '3':
    if n1 == n2:
        print("Resta de A y B: ")
        print(resta(matrixA, matrixB))
    else:
        print("Las matrices no tienen la misma dimensión, no es posible restar")
elif opcion == '4':
    if n1 == n2:
        print("Producto punto de A y B: ")
        print(prodPunto(matrixA, matrixB))
    else:
        print("Las matrices no tienen la misma dimensión, no es posible realizar el producto punto")
elif opcion == '5':
    esc = int(input("Ingrese el escalar a multiplicar"))
    opMat = (input("Digite 1 para multiplicar el escalar por A o 2 para B: "))
    if opMat == '1':
        print("Multiplicación de A*",esc,": ")
        print(multEscalar(matrixA, esc))
    elif opMat == '2':
        print("Multiplicación de B*",esc,": ")
        print(multEscalar(matrixB, esc))
    else:
        print("Opción no válida")
elif opcion == '6':
    opMat = (input("Digite 1 para hallar la inversa de A o 2 para la de B: "))
    if opMat == '1':
        print("Inversa de A")
        print(inversa(matrixA))
    elif opMat == '2':
        print("Inversa de B")
        print(inversa(matrixB))
    else:
        print("Opción no válida")
elif opcion == '7':
    opMat = (input("Digite 1 para hallar la transpuesta de A o 2 para la de B: "))
    if opMat == '1':
        print("Transpuesta de A")
        print(transpuesta(matrixA))
    elif opMat == '2':
        print("Transpuesta de B")
        print(transpuesta(matrixB))
    else:
        print("Opción no válida")
