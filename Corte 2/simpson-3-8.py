import tkinter as tk # Librería para la interfaz gráfica
from tkinter import messagebox as msg # Función de la librería para poder mandar alertas de error
import sympy as sp
import random
x = sp.symbols('x')

# Instanciar la ventana
app = tk.Tk()
# Tamaño de la ventana
app.geometry('320x250')

# Función para borrar todos los textos
def borrar():
    funcBox.delete(0, tk.END)
    extDerBox.delete(0, tk.END)
    extIzqBox.delete(0, tk.END)
    subIntervBox.delete(0, tk.END)
    solucionOutLabel['text'] = ''
    errorOutLabel['text'] = ''

# Función que retorna la función evaluada en un punto
def f(y, punto):
    return y.subs(x,punto)

# Funciones de la propia calculadora
# A todas las funciones les entran los mismos parámetros:
# funcion = la función f(x)
# a = el extremo izquierdo
# b = el extremo derecho
# n = el número de particiones/rectángulos
# h = la distancia entre intervalos
def solucion(funcion,a,b,n,h):
    intervalo = h
    area = 0
    i = a

    while(i+3*h <= b):
        area += f(funcion, i)+3*f(funcion,i+h)+3*f(funcion,i+2*h)+f(funcion,i+3*h)
        i+=3*h

    integral = area * (3*h/8)
    solucionOutLabel['text'] = integral

def error(funcion,a,b,h):
    ep = random.random()*(b-a)+a
    x = sp.Symbol('x')
    der = funcion
    for i in range(4):
        der = sp.Derivative(der, x, evaluate=True)

    error = -3*(h**5)*f(der,ep)/80
    errorOutLabel['text'] = error
    # print('epsilon: ', ep)

# Función que llama a las funciones de la calculadora, pero primero verifica que todos los datos sean válidos
def calcular():
    try:
        funcion = sp.sympify(funcBox.get())
        a = float(extIzqBox.get())
        b = float(extDerBox.get())
        n = int(subIntervBox.get())
        if n%3 == 1:
            n+=2
        elif n%3 == 2:
            n+=1
        h = (b-a)/n
    except:
        msg.showerror(title = 'Error', message = 'Hubo un error en el ingreso de datos, intente de nuevo')
    else:
        if a >= b:
            (msg.showerror(title = 'Error', message = 'El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo'))            
        else:
            solucion(funcion,a,b,n,h)
            error(funcion,a,b,h)
            
        

# Se definen las variables de la interfaz
title = tk.Label(app, text = 'Integración numérica por Simpson 3/8')
funcLabel = tk.Label(app, text = 'Función f(x) = ')
funcBox = tk.Entry(app)
extIzqLabel = tk.Label(app, text = 'Extremo Izquierdo: ')
extIzqBox = tk.Entry(app)
extDerLabel = tk.Label(app, text = 'Extremo Derecho: ')
extDerBox = tk.Entry(app)
subIntervLabel = tk.Label(app, text = 'Número de particiones:')
subIntervBox = tk.Entry(app)
calcBtn = tk.Button(text = 'Calcular', command = calcular)
eraseBtn = tk.Button(text = 'Borrar', command = borrar)
exitBtn = tk.Button(text = 'Salir', command = exit)
outputLabel = tk.Label(app, text = 'Salida: ')
solucionLabel = tk.Label(app, text = 'Simpson 3/8: ')
solucionOutLabel = tk.Label(app)
errorLabel = tk.Label(app, text = 'Error de formula: ')
errorOutLabel = tk.Label(app)

# Se ponen las variables en la interfaz
title.grid(columnspan = 3)
funcLabel.grid(row = 1, column = 0)
funcBox.grid(row = 1, column = 1)
extIzqLabel.grid(row = 2, column = 0)
extIzqBox.grid(row = 2, column = 1)
extDerLabel.grid(row = 3, column = 0)
extDerBox.grid(row = 3, column = 1)
subIntervLabel.grid(row = 4, column = 0)
subIntervBox.grid(row = 4, column = 1)
calcBtn.grid(row = 5, column = 0)
eraseBtn.grid(row = 5, column = 1)
exitBtn.grid(row = 5, column = 2)
outputLabel.grid()
solucionLabel.grid(row = 8, column = 0)
solucionOutLabel.grid(row = 8, column = 1)
errorLabel.grid(row = 9, column = 0)
errorOutLabel.grid(row = 9, column = 1)



app.mainloop()