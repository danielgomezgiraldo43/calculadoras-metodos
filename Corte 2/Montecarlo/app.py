from flask import Flask, render_template, request
import montecarlo as mnt
import sympy as sp

app = Flask(__name__)

@app.route('/', methods=['GET','POST'])
def index():
    if request.method == "POST":
        x = sp.Symbol('x')
        resultado = 0.0
        funcion = sp.sympify(request.form['funcion'])
        izq = float(sp.sympify(request.form['izq']))
        der = float(sp.sympify(request.form['der']))
        maximo = float(sp.sympify(request.form['maximo']))
        puntos = int(request.form['puntos'])
        if 'Calcular' in request.form:
            resultado = mnt.int_montecarlo(funcion,izq,der,maximo,puntos)
            return render_template('montecarlo.html',funcion=funcion, izq=izq, der=der, maximo=maximo, puntos=puntos, resultado=resultado)
        elif 'Borrar' in request.form:
            return render_template('index.html')
    return render_template('index.html')

# def suma(numero1, numero2):
#     return numero1+numero2

# def resta(numero1, numero2):
#     return numero1-numero2

if __name__ == '__main__':
    app.run(debug=True, port=4000)