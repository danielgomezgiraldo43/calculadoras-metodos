import tkinter as tk # Librería para la interfaz gráfica
from tkinter import messagebox as msg # Función de la librería para poder mandar alertas de error
import sympy as sp
x = sp.symbols('x')

# Instanciar la ventana
app = tk.Tk()
# Tamaño de la ventana
app.geometry('320x250')

# Función para borrar todos los textos
def borrar():
    funcBox.delete(0, tk.END)
    extDerBox.delete(0, tk.END)
    extIzqBox.delete(0, tk.END)
    subIntervBox.delete(0, tk.END)
    puntoIzqOutLabel['text'] = ''
    puntoDerOutLabel['text'] = ''
    puntoMedOutLabel['text'] = ''

# Función que retorna la función evaluada en un punto
def f(y, punto):
    return y.subs(x,punto)

# Funciones de la propia calculadora
# A todas las funciones les entran los mismos parámetros:
# funcion = la función f(x)
# a = el extremo izquierdo
# b = el extremo derecho
# n = el número de particiones/rectángulos
# h = la distancia entre intervalos
def puntoIzquierdo(funcion,a,b,n,h):
    intervalo = h
    area = f(funcion,a)

    for i in range(n-1):
        area += f(funcion,a+intervalo)
        intervalo += h

    integral = area * h
    puntoIzqOutLabel['text'] = integral

def puntoDerecho(funcion,a,b,n,h):
    intervalo = h
    area = f(funcion,b)

    for i in range(n-1):
        area += f(funcion,a+intervalo)
        intervalo += h

    integral = area * h
    puntoDerOutLabel['text'] = integral

def puntoMedio(funcion,a,b,n,h):
    intervalo = h/2
    area = f(funcion,a+intervalo)

    for i in range(n-1):
        intervalo += h
        area += f(funcion,a+intervalo)

    integral = area * h
    puntoMedOutLabel['text'] = integral

# Función que llama a las funciones de la calculadora, pero primero verifica que todos los datos sean válidos
def calcular():
    try:
        funcion = sp.sympify(funcBox.get())
        a = float(extIzqBox.get())
        b = float(extDerBox.get())
        n = int(subIntervBox.get())
        h = (b-a)/n
    except:
        msg.showerror(title = 'Error', message = 'Hubo un error en el ingreso de datos, intente de nuevo')
    else:
        if a >= b:
            (msg.showerror(title = 'Error', message = 'El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo'))            
        else:
            puntoIzquierdo(funcion,a,b,n,h)
            puntoDerecho(funcion,a,b,n,h)
            puntoMedio(funcion,a,b,n,h)
            
        

# Se definen las variables de la interfaz
title = tk.Label(app, text = 'Integración numérica por rectángulos')
funcLabel = tk.Label(app, text = 'Función f(x) = ')
funcBox = tk.Entry(app)
extIzqLabel = tk.Label(app, text = 'Extremo Izquierdo: ')
extIzqBox = tk.Entry(app)
extDerLabel = tk.Label(app, text = 'Extremo Derecho: ')
extDerBox = tk.Entry(app)
subIntervLabel = tk.Label(app, text = 'Número de particiones:')
subIntervBox = tk.Entry(app)
calcBtn = tk.Button(text = 'Calcular', command = calcular)
eraseBtn = tk.Button(text = 'Borrar', command = borrar)
exitBtn = tk.Button(text = 'Salir', command = exit)
outputLabel = tk.Label(app, text = 'Salida: ')
aproxLabel = tk.Label(app, text = 'Aproximaciones: ')
puntoIzqLabel = tk.Label(app, text = 'Punto izquierdo: ')
puntoIzqOutLabel = tk.Label(app)
puntoDerLabel = tk.Label(app, text = 'Punto derecho: ')
puntoDerOutLabel = tk.Label(app)
puntoMedLabel = tk.Label(app, text = 'Punto medio: ')
puntoMedOutLabel = tk.Label(app)

# Se ponen las variables en la interfaz
title.grid(columnspan = 3)
funcLabel.grid(row = 1, column = 0)
funcBox.grid(row = 1, column = 1)
extIzqLabel.grid(row = 2, column = 0)
extIzqBox.grid(row = 2, column = 1)
extDerLabel.grid(row = 3, column = 0)
extDerBox.grid(row = 3, column = 1)
subIntervLabel.grid(row = 4, column = 0)
subIntervBox.grid(row = 4, column = 1)
calcBtn.grid(row = 5, column = 0)
eraseBtn.grid(row = 5, column = 1)
exitBtn.grid(row = 5, column = 2)
outputLabel.grid()
aproxLabel.grid()
puntoIzqLabel.grid(row = 8, column = 0)
puntoIzqOutLabel.grid(row = 8, column = 1)
puntoDerLabel.grid(row = 9, column = 0)
puntoDerOutLabel.grid(row = 9, column = 1)
puntoMedLabel.grid(row = 10, column = 0)
puntoMedOutLabel.grid(row = 10, column = 1)



app.mainloop()